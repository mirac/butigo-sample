Butigo Sample Application
==============
Summary
--------------
This is sample project for using example of multiple projects in Django framework.
"butigo_core" and "butigo_fronted" are, the two projects joined using custom python codes.

**Used technologies;**

- Python
- Django
- Tastypie
- JQuery Ajax API
- Django-mptt
- Slumber Tastypie Client