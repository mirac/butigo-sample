# -*- coding: utf-8 -*-
from django import forms
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import RequestContext, loader
from django.views.decorators.csrf import csrf_exempt
# butigo_core uygulamamızdan CatalogueForm model form objemizi import ediyoruz
from butigo_core.catalogue.forms import CatalogueForm

import slumber
import json

# django_core uygulamasının sağladığı API adresimiz
API_URL = 'http://localhost:8000/api/v1/'


def index(request):
    """
    Boş bir Katalog oluşturup template motoruna set ederek
    anasayfayı render ediyoruz.
    :param request:
    :return:
    """
    form = CatalogueForm()
    template = loader.get_template('catalogue/index.html')
    context = RequestContext(request, {
        'form': form
    })

    return HttpResponse(template.render(context))


def getAllData(request):
    """
    API'dan katalog listesini çekip servis ediyoruz
    :param request:
    :return:
    """
    api = slumber.API(API_URL)
    catalogues = api.catalogue.get()

    return HttpResponse(json.dumps(catalogues), content_type="application/json")

@csrf_exempt
def save(request):
    """
    Bu metotta POST metoduyla gelen veriyi, sunucu taraflı
    form validasyonu yaparak API'a gönderiyoruz.
    :param request:
    :return:
    """
    if request.method == 'POST':
        api = slumber.API(API_URL)
        form = CatalogueForm(request.POST)

        if form.is_valid():
            isim  = request.POST["isim"]
            adres = request.POST["adres"]
            print isim, adres
            api.catalogue.post({"isim": isim, "adres": adres})
        else:
            form = CatalogueForm()
            raise forms.ValidationError("Form dogrulamasi basarisiz!")

        return redirect('butigo_frontend.views.index')
    else:
        return HttpResponse('Unauthorized', status=401)

@csrf_exempt
def delete(request, id):
    """
    Katalog ID'sini alarak ve istek yönetiminin DELETE olduğunu
    doğrulayarak silme isteğini API'a iletiyoruz.
    :param request:
    :param id:
    :return:
    """
    if request.method == 'DELETE' and request.is_ajax():
        api = slumber.API(API_URL)

        if(id is not None):
            api.catalogue(id).delete()
        else:
            return HttpResponse("Katalog silinemedi!")

    return HttpResponse("Katalog başarıyla silindi!")




















