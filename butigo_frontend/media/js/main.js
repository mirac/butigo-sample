            $(function() {
               // Sayfa ilk yüklendiğinde katalog listesini çekiyoruz
               getCatalogueList();
            });

            // Javascript uyuma fonksiyonu, veri gelmeden mesaj gösterilmesin diye kullanılıyor.
            function sleep(milliseconds) {
              var start = new Date().getTime();
              for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                  break;
                }
              }
            }

            // Kataloğu silmek için View'a istek gönderiyoruz o da API'ya iletiyor
            function deleteCatalogue(id) {
                $.ajax({
                   type: 'DELETE',
                   contentType: 'application/json',
                   url: '/delete/' + id + '/',

                   success: function() {
                       $('span').empty();
                       $('#sonuc').hide();
                       $("#sonuc span").append("Katalog başarıyla silindi!");
                       $('#sonuc').fadeIn(300);
                   }
                });

                sleep(300);
                getCatalogueList();
            }

            // Katalog listesini çekip listeye yazdırıyoruz
            function getCatalogueList() {
                $.ajax({
                   type: 'GET',
                   contentType: 'application/json',
                   url: '/data/',

                    success: function(response) {
                        $('ul').empty();
                        var data = response.objects;
                        var items = [];

                        $.each(data, function(c, cat) {
                           items.push('<li>' + data[c].isim + ' | ' + data[c].adres + ' <small>(<a href="javascript:deleteCatalogue('+data[c].id+');">Sil</a>)</small></li>')
                        });
                        $('#cats').append(items.join(''));
                    }

                });

            }