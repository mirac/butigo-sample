from django.conf.urls import patterns, url, include
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',

    url(r'^$', 'butigo_frontend.views.index'),
    url(r'^save/', 'butigo_frontend.views.save'),
    url(r'^data/', 'butigo_frontend.views.getAllData'),

    url(r'^delete/(?P<id>\d+)/$', 'butigo_frontend.views.delete'),
)

urlpatterns += staticfiles_urlpatterns()

urlpatterns += patterns('',
    url(r'^media/(?P<path>.*)$',
        'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, }),
)