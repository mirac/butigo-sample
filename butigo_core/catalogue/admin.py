from django.contrib import admin
from catalogue.models import Catalogue
#from mptt.admin import MPTTModelAdmin


#class CatalogueAdmin(MPTTModelAdmin):
 #   mptt_indent_field = "isim"
  #  mptt_level_indent = 20

   # list_display = ('isim',)


#admin.site.register(Catalogue, CatalogueAdmin)

class CatalogueAdmin(admin.ModelAdmin):
    list_display = ('isim',)


admin.site.register(Catalogue, CatalogueAdmin)