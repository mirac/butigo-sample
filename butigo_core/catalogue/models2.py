from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Catalogue(MPTTModel):
    isim   = models.CharField(max_length=100)
    adres  = models.CharField(max_length=100)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='subCatalogue')

    class Meta:
        app_label = "catalogue"

    class MPTTMeta:
        order_insertion_by = ['isim']

    def __unicode__(self):
        return self.isim