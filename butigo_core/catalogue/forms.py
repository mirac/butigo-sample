from django.forms import ModelForm
from butigo_core.catalogue.models import Catalogue


class CatalogueForm(ModelForm):

    class Meta:
        model = Catalogue