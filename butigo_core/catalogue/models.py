from django.db import models


class Catalogue(models.Model):
    isim = models.CharField(max_length=100)
    adres = models.CharField(max_length=100)

    def __unicode__(self):
        return self.isim

    class Meta:
        app_label = "catalogue"